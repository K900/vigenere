use std::collections::{HashMap, HashSet};
use std::env;
use std::fs::File;
use std::hash::Hash;
use std::io::{self, BufRead, BufReader, Read};

extern crate itertools;
use itertools::Itertools;

extern crate rayon;
use rayon::prelude::*;

#[derive(Debug)]
pub struct Alphabet {
    pub letters: HashMap<char, f64>,
    pub indices: HashMap<char, u32>,
    pub chars: Vec<char>,
    pub freqs: Vec<f64>,
    pub length: u32,
}

impl Alphabet {
    pub fn new(alphabet: &[(char, f64)]) -> Alphabet {
        let mut letters = HashMap::new();
        let mut indices = HashMap::new();

        let mut freqs = Vec::new();
        let mut chars = Vec::new();

        let mut i = 0u32;

        for &(letter, freq) in alphabet {
            let lower = Alphabet::lower(letter);
            let upper = Alphabet::upper(letter);
            letters.insert(lower, freq);

            indices.insert(lower, i);
            indices.insert(upper, i);
            i += 1;

            freqs.push(freq);
            chars.push(letter);
        }

        Alphabet {
            length: letters.len() as u32,
            letters: letters,
            indices: indices,
            freqs: freqs,
            chars: chars,
        }
    }

    fn lower(letter: char) -> char {
        letter.to_lowercase().next().expect("Can't lowercase letter!")
    }

    fn upper(letter: char) -> char {
        letter.to_uppercase().next().expect("Can't uppercase letter!")
    }

    pub fn contains(&self, letter: char) -> bool {
        self.letters.contains_key(&Alphabet::lower(letter))
    }

    fn modulo_sub(&self, x: u32, y: u32) -> u32 {
        let n = self.length;
        (((x % n) as i32 - (y % n) as i32 + n as i32) % n as i32) as u32
    }

    pub fn index(&self, letter: char) -> u32 {
        self.indices.get(&letter).expect(&format!("Letter not in alphabet: {:?}!", letter)).clone()
    }

    pub fn from_index(&self, idx: usize) -> char {
        self.chars[idx]
    }

    pub fn distance(&self, x: char, y: char) -> u32 {
        self.modulo_sub(self.index(x), self.index(y))
    }

    pub fn shift(&self, letter: char, key: char) -> char {
        self.from_index((self.distance(letter, key)) as usize)
    }

    pub fn english() -> Alphabet {
        Alphabet::new(&[('a', 0.084),
                        ('b', 0.023),
                        ('c', 0.021),
                        ('d', 0.046),
                        ('e', 0.116),
                        ('f', 0.020),
                        ('g', 0.025),
                        ('h', 0.049),
                        ('i', 0.076),
                        ('j', 0.020),
                        ('k', 0.050),
                        ('l', 0.038),
                        ('m', 0.034),
                        ('n', 0.066),
                        ('o', 0.066),
                        ('p', 0.015),
                        ('q', 0.002),
                        ('r', 0.064),
                        ('s', 0.073),
                        ('t', 0.081),
                        ('u', 0.019),
                        ('v', 0.011),
                        ('w', 0.021),
                        ('x', 0.002),
                        ('y', 0.024),
                        ('z', 0.003)])
    }

    pub fn russian() -> Alphabet {
        Alphabet::new(&[('а', 0.080),
                        ('б', 0.016),
                        ('в', 0.045),
                        ('г', 0.017),
                        ('д', 0.030),
                        ('е', 0.084),
                        ('ё', 0.00013),
                        ('ж', 0.009),
                        ('з', 0.016),
                        ('и', 0.074),
                        ('й', 0.012),
                        ('к', 0.035),
                        ('л', 0.043),
                        ('м', 0.032),
                        ('н', 0.067),
                        ('о', 0.109),
                        ('п', 0.028),
                        ('р', 0.047),
                        ('с', 0.055),
                        ('т', 0.063),
                        ('у', 0.026),
                        ('ф', 0.003),
                        ('х', 0.010),
                        ('ц', 0.005),
                        ('ч', 0.015),
                        ('ш', 0.007),
                        ('щ', 0.004),
                        ('ъ', 0.0003),
                        ('ы', 0.019),
                        ('ь', 0.017),
                        ('э', 0.003),
                        ('ю', 0.006),
                        ('я', 0.020)])
    }
}

pub struct Cracker<'a> {
    alphabet: Alphabet,
    words: HashSet<String>,
    ciphertext: &'a str,
    normalized: String,
    threshold: f64,
}

impl<'a> Cracker<'a> {
    pub fn new(ciphertext: &'a str,
               alphabet: Alphabet,
               words: HashSet<String>,
               threshold: f64)
               -> Cracker<'a> {
        Cracker {
            words: words,
            ciphertext: ciphertext,
            threshold: threshold,
            normalized: ciphertext.chars()
                                  .filter_map(|letter| letter.to_lowercase().next())
                                  .filter(|&letter| alphabet.contains(letter))
                                  .collect(),
            // order matters here
            alphabet: alphabet,
        }
    }

    pub fn crack(&mut self) {
        println!("Generating keys...");
        let keys: Vec<_> = self.key_lengths().iter().flat_map(|x| self.generate_keys(*x)).collect();
        println!("Starting brute force attack...");
        let results: Vec<_> = keys.iter().map(|key| {
            let plaintext = self.decrypt(&key);
            let word_rate = self.valid_word_rate(&plaintext);
            if word_rate >= self.threshold {
                println!("------------------------------------------------");
                println!("Found possible solution! Key: {}", key);
                println!("Word match rate: {}", word_rate);
                println!("Decrypted text:");
                println!("{}", plaintext);
                println!("------------------------------------------------");
                1
            } else {
                0
            }
        }).collect();
    }

    fn key_lengths(&self) -> Vec<usize> {
        let chars: Vec<char> = self.normalized.chars().collect();

        let mut ngrams: HashMap<&[char], Vec<usize>> = HashMap::new();

        for (idx, ngram) in chars.windows(3).enumerate() {
            ngrams.entry(ngram).or_insert(Vec::new()).push(idx);
        }

        let indices = ngrams.into_iter().filter_map(|(_, v)| {
            if v.len() > 1 {
                Some(v)
            } else {
                None
            }
        });

        let mut factors = HashMap::new();

        for x in indices {
            for w in x.windows(2) {
                for f in factorize(w[1] - w[0]) {
                    *factors.entry(f).or_insert(0) += 1;
                }
            }
        }

        most_common_keys(factors)
    }

    fn generate_keys(&self, length: usize) -> Vec<String> {
        let mut results = Vec::new();
        for word in &self.words {
            if word.chars().count() == length {
                results.push(word.clone());
            }
        }
        results
    }

    fn decrypt(&self, key: &'a str) -> String {
        let mut result = String::new();
        let mut key_iter = key.chars().cycle();
        let mut key_char = key_iter.next().expect("Empty key!");

        for ch in self.ciphertext.chars() {
            if self.alphabet.contains(ch) {
                result.push(self.alphabet.shift(ch, key_char));
                key_char = key_iter.next().expect("Key cycle ended?!?!?!");
            } else {
                result.push(ch);
            }
        }

        result
    }

    fn valid_word_rate(&self, text: &str) -> f64 {
        let mut valid = 0f64;
        let mut total = 0f64;

        for word in text.split(' ') {
            if self.words.contains(word) {
                valid += 1f64;
            }
            total += 1f64;
        }

        valid / total
    }
}

fn factorize(n: usize) -> Vec<usize> {
    let mut result = Vec::new();
    for x in 1..(n as f64).sqrt() as usize {
        if n % x == 0 {
            result.push(x);
            result.push(n / x);
        }
    }
    result
}

fn most_common_keys<K, V>(map: HashMap<K, V>) -> Vec<K>
    where K: Hash + Eq,
          V: PartialOrd
{
    map.into_iter()
       .sorted_by(|a, b| b.1.partial_cmp(&a.1).expect("Can't compare floats!"))
       .into_iter()
       .map(|(x, _)| x)
       .collect()
}

fn read_wordlist(path: &str) -> HashSet<String> {
    let mut result = HashSet::new();
    let f = File::open(path).expect("Can't open word list!");
    let b = BufReader::new(f);
    for line in b.lines() {
        if let Ok(line) = line {
            result.insert(line.trim().into());
        }
    }
    result
}

fn contains_letters(cyphertext: &str, alphabet: &Alphabet) -> bool {
    for ch in cyphertext.chars() {
        if alphabet.contains(ch) {
            return true;
        }
    }
    false
}

fn guess_language(cyphertext: &str) -> (Alphabet, HashSet<String>) {
    let russian = Alphabet::russian();
    if contains_letters(cyphertext, &russian) {
        (russian, read_wordlist("words_ru.txt"))
    } else {
        (Alphabet::english(), read_wordlist("words_en.txt"))
    }
}

fn main() {
    let args: Vec<_> = env::args().collect();

    let mut cyphertext = String::new();
    if args.len() > 1 {
        let f = File::open(&args[1]).expect("Can't open cyphertext file!");
        let b = BufReader::new(f);

        for line in b.lines() {
            if let Ok(line) = line {
                cyphertext.push_str(&line);
                cyphertext.push(' ');
            }
        }
        cyphertext = cyphertext.trim().to_string();
    } else {
        println!("Enter cyphertext:");
        io::stdin().read_line(&mut cyphertext).expect("Failed to read stdin!");
    }

    println!("Loading word lists...");
    let (alpha, words) = guess_language(&cyphertext);

    println!("Starting decryption...");
    Cracker::new(&cyphertext, alpha, words, 0.5f64).crack();
}
