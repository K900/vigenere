from __future__ import division, print_function

from collections import defaultdict
from itertools import cycle
from math import log
import re

FREQS = {
    'A': 84, 'B': 23, 'C': 21, 'D': 46, 'E': 116, 'F': 20, 'G': 25, 'H': 49, 'I': 76,
    'J': 2, 'K': 5, 'L': 38, 'M': 34, 'N': 66, 'O': 66, 'P': 15, 'Q': 2, 'R': 64,
    'S': 73, 'T': 81, 'U': 19, 'V': 11, 'W': 21, 'X': 2, 'Y': 24, 'Z': 3
}

for k in FREQS:
    FREQS[k] /= 1000.0

ALPHABET = list(FREQS.keys())

with open('/usr/share/dict/words') as f:
    WORDS = [word.strip('\n').lower() for word in f.readlines()]


def a_idx(x):
    return ord(x) - ord(ALPHABET[0].upper())


def precalc_bans():
    bans = {}
    for a in ALPHABET:
        x = (25 * FREQS[a]) / (1 - FREQS[a])
        x = log(x) / log(10)
        bans[a_idx(a)] = x

    return bans


BANS = precalc_bans()


def a_diff(x, y):
    return (a_idx(x) - a_idx(y)) % len(ALPHABET)


def top_keys(d):
    return sorted(d, key=d.__getitem__, reverse=True)


def guess_keys(columns, count):
    # TODO
    column_letters = []
    counts = []
    for ranks in columns:
        column_letters.append(top_keys(ranks))
        counts.append(0)

    results = []
    result_count = 0
    while result_count < count:
        best_pass = ""
        smallest_diff = 1000
        smallest_col = -1
        for i in range(0, len(columns)):
            best_pass += column_letters[i][counts[i]]
            if counts[i] < 25:
                v1 = columns[i][column_letters[i][counts[i]]]
                v2 = columns[i][column_letters[i][counts[i] + 1]]
                diff = v1 - v2
                if diff < smallest_diff:
                    smallest_diff = diff
                    smallest_col = i
        counts[smallest_col] += 1
        results.append(best_pass)
        result_count += 1
    return results


def turing_check(cyphertext, key_length, result_count):
    by_letter = defaultdict(list)

    for letter in ALPHABET:
        for column in range(0, key_length):
            evidence = 0
            for i in range(column, len(cyphertext), key_length):
                diff = a_diff(cyphertext[i], letter)
                evidence += BANS[diff]
            by_letter[letter].append(evidence)

    results = []
    for i in range(0, key_length):
        column = {}
        for l in ALPHABET:
            column[l] = by_letter[l][i]
        results.append(column)

    return guess_keys(results, result_count)


def _decrypt(cyphertext, key):
    key = cycle(key)
    key_char = next(key)

    for letter in cyphertext:
        if letter in ALPHABET:
            yield chr(a_diff(letter, key_char) + ord(ALPHABET[0].upper()))
            key_char = next(key)
        else:
            yield letter


def decrypt(cyphertext, key):
    return ''.join(_decrypt(cyphertext, key))


def factorize(n):
    yield 1
    yield n
    for i in range(2, int(n ** 0.5)):
        if n % i == 0:
            yield i
            yield n // i


def find_ngrams(cyphertext, substring_len):
    """ -> dict<ngram, list<start_idx>>"""
    indices = {}

    for i in range(0, len(cyphertext) - substring_len):
        ngram = cyphertext[i:i + substring_len]

        if ngram not in indices and cyphertext.count(ngram) > 1:
            indices[ngram] = [m.start() for m in re.finditer(ngram, cyphertext)]

    return indices


def key_lengths(cyphertext):
    steps = []

    for substring_indices in find_ngrams(cyphertext, 3).values():
        for this, that in zip(substring_indices[1:], substring_indices):
            steps.append(this - that)

    factor_counts = defaultdict(int)
    for step in steps:
        for factor in factorize(step):
            factor_counts[factor] += 1

    return top_keys(factor_counts)


def solutions(cyphertext, key_count):
    normalized = ''.join(c for c in cyphertext if c in ALPHABET)

    for length in key_lengths(normalized):
        likely_keys = turing_check(normalized, length, key_count)
        for key in likely_keys:
            plaintext = decrypt(cyphertext, key)
            yield key, plaintext


def check_words(plaintext):
    matched = 0
    total = 0
    for word in plaintext.split():
        total += 1
        if word.lower() in WORDS:
            matched += 1

    return matched / total


def crack(cyphertext, key_count, words_to_pass):
    for key, plaintext in solutions(cyphertext, key_count):
        if check_words(plaintext) >= words_to_pass:
            print("Found possible key: {}".format(key))
            print("Plaintext: {}".format(plaintext))


SECRET = 'XT XAFS LWPM CCPJI FV BD BSM CCPJI FV JPGX QYCC CXAVP ECXMP EGWIYJ FPJI WFLT' \
         ' HLXZP HDMMV ZJH MG KFT CRLVR RCQX JM HVEEC G IOWMV YI TMKJR IVI OVPN KSKJR ' \
         'DT JHIRJBI L DGVVX TEB DHLXI QIFEBEQ DT AHV UWWGA EML GIXD UDS GHDNPFIW NGIV' \
         ' PHJQ DT XAVC LWPE EMI GIXD QD'

crack(SECRET, 10, 0.8)
